//TEST
// ==UserScript==
// @name         Netflix Romanian Subtitle Resizer
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  A simple script that will resize Netflix image based subtitles!
// @author       Adrian Axente adrianaxente@yahoo.com
// @match        https://www.netflix.com/*
// @grant        none
// ==/UserScript==

// MIT Licensed
// Author: jwilson8767

/**
 * Waits for an element satisfying selector to exist, then resolves promise with the element.
 * Useful for resolving race conditions.
 *
 * @param selector
 * @returns {Promise}
 */
function elementReady(selector) {
  return new Promise((resolve, reject) => {
    const el = document.querySelector(selector);
    if (el) {resolve(el);}
    new MutationObserver((mutationRecords, observer) => {
      // Query for elements matching the specified selector
      Array.from(document.querySelectorAll(selector)).forEach((element) => {
        resolve(element);
        //Once we have resolved we don't need the observer anymore.
        observer.disconnect();
      });
    })
      .observe(document.documentElement, {
        childList: true,
        subtree: true
      });
  });
}

(function() {
    'use strict';
    const imageBasedTimedTextPercent = 50;
    elementReady('.image-based-timed-text')
        .then(
           (element)=>{
               console.log("Netflix Romanian subtitle resized to: " + imageBasedTimedTextPercent + "%");
               element.style.top = (100 - imageBasedTimedTextPercent) + "%";
            });
})();